-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Tempo de geração: 06/06/2018 às 20:44
-- Versão do servidor: 5.6.32-78.1
-- Versão do PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `socia158_meusite`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `contatos`
--

CREATE TABLE IF NOT EXISTS `contatos` (
  `codigo` int(11) NOT NULL,
  `nomeCompleto` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `cidade` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `genero` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(11) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `contatos`
--

INSERT INTO `contatos` (`codigo`, `nomeCompleto`, `endereco`, `cidade`, `genero`, `telefone`) VALUES
(2, 'Taironne Matos', 'Av. EpitÃ¡cio Pessoa, 88', 'JoÃ£o Pessoa', 'Masculino', '83988762345'),
(3, 'Maria dos Santos', 'Rua Marginal Pinheiro, 132', 'Recife', 'Feminino', '81998672249'),
(7, 'Duanne Matos', 'Av. EpitÃ¡cio Pessoa, 188', 'JoÃ£o Pessoa', 'Feminino', '83987761327');

-- --------------------------------------------------------

--
-- Estrutura para tabela `falecom`
--

CREATE TABLE IF NOT EXISTS `falecom` (
  `codigo` int(11) NOT NULL,
  `nome` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `assunto` text COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Fazendo dump de dados para tabela `falecom`
--

INSERT INTO `falecom` (`codigo`, `nome`, `email`, `assunto`, `mensagem`) VALUES
(1, 'Taironne Matos', 'taironne.matos@gmail.com', 'Projeto PHP', 'A listagem de Mensagem foi anexada para validar o funcionamento com o banco de dados.');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `ID` int(10) unsigned zerofill NOT NULL,
  `email` varchar(80) NOT NULL,
  `senha` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `usuarios`
--

INSERT INTO `usuarios` (`ID`, `email`, `senha`) VALUES
(0000000001, 'teste@teste.com', 'e10adc3949ba59abbe56e057f20f883e'),
(0000000002, 'taironne@taironne.com', '25d55ad283aa400af464c76d713c07ad'),
(0000000003, 'matos@matos.com', '25d55ad283aa400af464c76d713c07ad');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `contatos`
--
ALTER TABLE `contatos`
  ADD PRIMARY KEY (`codigo`);

--
-- Índices de tabela `falecom`
--
ALTER TABLE `falecom`
  ADD PRIMARY KEY (`codigo`);

--
-- Índices de tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `contatos`
--
ALTER TABLE `contatos`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de tabela `falecom`
--
ALTER TABLE `falecom`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
