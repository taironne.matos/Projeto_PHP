<?php
@ob_start();
session_start();

include_once("conexao.php");

if (!isset($_SESSION['usuario'])) {
  header("Location:login.php");
}

$contatos = mysqli_query($conexao, "SELECT * FROM contatos");
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Agenda de Cantatos</title>
    <!-- Required meta tags -->
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="css/estilo.css" rel="stylesheet">
  </head>
  <body>  
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Agenda de Contatos</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quem_somos.html">Quem Somos <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="faleconosco.php">Fale Conosco <span class="sr-only">(current)</span></a>
            </li>
          </ul>
          <a href="logout.php">Sair</a>
        </div>
      </nav>
    </header>

    <div class="container">
      <div class="row">
        <main role="main" class="col-sm-9 ml-sm-auto col-md-12 pt-3">
          <?php
            if (isset($_SESSION['cadastrado'])) {
              if ($_SESSION['cadastrado'] == true) {
                echo "<div class='alert alert-success' role='alert'>" .
                        "Contato salvo com sucesso" .
                        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" .
                          "<span aria-hidden='true'>&times;</span>" .
                        "</button>" .
                      "</div>";
              } else {
                echo "<div class='alert alert-danger' role='alert'>" .
                        "Erro ao tentar salvar o contato." .
                        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" .
                          "<span aria-hidden='true'>&times;</span>" .
                        "</button>" .
                      "</div>";
              }
            }
            if (isset($_SESSION['deletado'])) {
              if ($_SESSION['deletado'] == true) {
                echo "<div class='alert alert-success' role='alert'>" .
                        "Contato deletado com sucesso" .
                        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" .
                          "<span aria-hidden='true'>&times;</span>" .
                        "</button>" .
                      "</div>";
              } else {
                echo "<div class='alert alert-danger' role='alert'>" .
                        "Erro ao tentar deletar o contato." .
                        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" .
                          "<span aria-hidden='true'>&times;</span>" .
                        "</button>" .
                      "</div>";
              }
            }
            unset($_SESSION['cadastrado']);
            unset($_SESSION['deletado']);
          ?>
          <h1>Gerenciar Contato</h1>
          <section>
            <form id="form" method="POST", action="envia_contato.php">
              <input type="hidden" name="codigo" id="codigo"></input>
              <div class="form-group">
                <div class="form-group">
                  <label for="nomeCompleto">Nome Completo</label>
                  <input required type="text" name="nomeCompleto" class="form-control" id="nomeCompleto" placeholder="Ex: Maria dos Satos">
                </div>
              </div>
              <div class="form-group">
                <label for="endereco">Endereço</label>
                <input required type="text" name="endereco" class="form-control" id="endereco" placeholder="Ex: Av. Epitácio Pessoa, 77">
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="cidade">Cidade</label>
                  <input required type="text" name="cidade" class="form-control" id="cidade" placeholder="Ex: João Pessoa">
                </div>
                <div class="form-group col-md-4">
                  <label for="genero">Gênero</label>
                  <select required name="genero" id="genero" class="form-control">
                    <option value="Masculino" selected>Masculino</option>
                    <option value="Feminino">Feminino</option>
                  </select>
                </div>
                <div class="form-group col-md-2">
                  <label for="telefone">Telefone</label>
                  <input required type="number" name="telefone" class="form-control" id="telefone" placeholder="Ex: 83999999999">
                </div>
              </div>
              <button id="submit" type="submit" class="btn btn-primary">Cadastrar</button>
              <button id="submit" type="button" class="btn btn-default" onClick="location.href=location.href">Cancelar</button>
            </form>
          </section>
          <hr>
          <h2>Lista de Contatos</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Nome Completo</th>
                  <th>Endereço</th>
                  <th>Cidade</th>
                  <th>Gênero</th>
                  <th>Telefone</th>
                  <th>Opções</th>
                </tr>
              </thead>
              <tbody>
                <?php while ($contato = mysqli_fetch_array($contatos)) : ?>
                  <tr data-codigo="<?= $contato['codigo']?>">
                    <td class="nomeCompleto"><?= $contato['nomeCompleto'] ?></td>
                    <td class="endereco"><?= $contato['endereco'] ?></td>
                    <td class="cidade"><?= $contato['cidade'] ?></td>
                    <td class="genero"><?= $contato['genero'] ?></td>
                    <td class="telefone"><?= $contato['telefone'] ?></td>
                    <td>
                      <a href="" class="btn btn-warning editar">Editar</a>
                      <a href="deletar.php?codigo=<?= $contato['codigo'] ?>" class="btn btn-danger">Deletar</a>
                    </td>
                  </tr>
                <?php endwhile ?>
              </tbody>
            </table>
          </div>
        </main>
      </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>

  <script>
    $(document).ready(function() {
      $(".editar").on("click", function(e) {
        e.preventDefault();
        var tr = $(this).parents('tr');
        var codigo = tr.data('codigo');
        var nomeCompleto = tr.children('.nomeCompleto').text();
        var endereco = tr.children('.endereco').text();
        var cidade = tr.children('.cidade').text();
        var genero = tr.children('.genero').text();
        var telefone = tr.children('.telefone').text();

        $("#codigo").val(codigo);
        $("#nomeCompleto").val(nomeCompleto);
        $("#endereco").val(endereco);
        $("#cidade").val(cidade);
        $("#genero").val(genero);
        $("#telefone").val(telefone);

        $("#submit").text("Salvar");
      });
    });
  </script>
</html>
