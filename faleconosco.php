<?php
@ob_start();
session_start();

include_once("conexao.php");

$falecom = mysqli_query($conexao, "SELECT * FROM falecom");
?>
<!doctype html>
<html lang="en">
  <head>
    <title>Contatos</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="css/estilo.css" rel="stylesheet">
  </head>
  <body>  
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Agenda de Contatos</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="quem_somos.html">Quem Somos <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="#">Fale Conosco <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <div class="container">
      <div class="row">
        <main role="main" class="col-sm-9 ml-sm-auto col-md-12 pt-3">
          <?php
            if (isset($_SESSION['enviado'])) {
              if ($_SESSION['enviado'] == true) {
                echo "<div class='alert alert-success' role='alert'>" .
                        "Email enviado com sucesso." .
                        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" .
                          "<span aria-hidden='true'>&times;</span>" .
                        "</button>" .
                      "</div>";
              } else {
                echo "<div class='alert alert-danger' role='alert'>" .
                        "Erro ao tentar enviar e-mail. Tente mais tarde." .
                        "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" .
                          "<span aria-hidden='true'>&times;</span>" .
                        "</button>" .
                      "</div>";
              }
            }
            unset($_SESSION['enviado']);
          ?>

          <h1>Fale Conosco</h1>
          <section>
            <form id="form" method="POST", action="envia_form.php">
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputPassword4">Nome</label>
                    <input required type="text" class="form-control" name="nome" id="inputPassword4" placeholder="Ex: Maria do Carmo">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="inputEmail4">Email</label>
                    <input required type="email" class="form-control" name="email" id="inputEmail4" placeholder="Ex: meuemail@gmail.com">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputAddress">Assunto</label>
                  <input required type="text" class="form-control" name="assunto" id="inputAddress" placeholder="Ex: Esclarecimento">
                </div>
                <div class="form-group">
                  <label for="exampleFormControlTextarea1">Mensagem</label>
                  <textarea class="form-control" name="mensagem" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
              </form>
          </section>
          <hr>

          <h2>Lista de Mensagens</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Email</th>
                  <th>Assunto</th>
                  <th>Mensagem</th>
                </tr>
              </thead>
              <tbody>
                <?php while ($form = mysqli_fetch_array($falecom)) : ?>
                  <tr data-codigo="<?= $form['codigo']?>">
                    <td class="nome"><?= $form['nome'] ?></td>
                    <td class="email"><?= $form['email'] ?></td>
                    <td class="assunto"><?= $form['assunto'] ?></td>
                    <td class="mensagem"><?= $form['mensagem'] ?></td>
                  </tr>
                <?php endwhile ?>
              </tbody>
            </table>
          </div>
        </main>
      </div>

  </body>

</html>
