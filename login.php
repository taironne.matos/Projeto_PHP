<?php
@ob_start();
session_start();

if ($_POST) {
  include_once("conexao.php");

  $email = $_POST['email'];
  $senha = md5($_POST['senha']);

  $query_select = "SELECT * FROM usuarios WHERE email = '$email' AND senha='$senha'";
  $select = mysqli_query($conexao, $query_select);

  if (mysqli_num_rows($select) > 0) {
    $_SESSION['usuario'] = true;
    header("Location: index.php");
  }

  $erroLogin = true;
}
?>

<!doctype html>
<html lang="en">
  <head>
    <title>Agenda de Cantatos - Login</title>
    <!-- Required meta tags -->
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="css/login.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <div class="container">
      <div class="row">
        <main role="main" class="col-sm-9 ml-sm-auto col-md-12 pt-3">
          <?php
          if (isset($erroLogin)) {
            echo "<div class='alert alert-danger' role='alert'>" .
                  "Login e/ou senha incorretos." .
                  "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" .
                    "<span aria-hidden='true'>&times;</span>" .
                  "</button>" .
                "</div>";
          }
          ?>
          <section>
            <form class="form-signin" method="POST" action="login.php">
              <h1 class="h3 mb-3 font-weight-normal">Por favor, faça login</h1>
              <label for="inputEmail" class="sr-only">Email</label>
              <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email" required autofocus>
              <label for="inputPassword" class="sr-only">Senha</label>
              <input type="password" name="senha" id="inputPassword" class="form-control" placeholder="Senha" required>
              <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
              <a href="cadastro.php">Cadastre-se</a>
            </form>
          </section>
        </main>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>
